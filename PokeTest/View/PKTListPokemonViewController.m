//
//  PKTListPokemonViewController.m
//  PokeTest
//
//  Created by Tiago Bencardino on 18/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTListPokemonViewController.h"
#import "PKTListPokemonViewModel.h"
#import "PKTCommonCollectionViewCell.h"
#import "PKTLoadingCollectionViewCell.h"

#import "UIImageView+AFNetworking.h"
#import "UIImageView+PKTClearCache.h"

NSString * const commonCellIdentifier = @"PKTCommonCollectionViewCell";
NSString * const loadingCellIdentifier = @"PKTLoadingCollectionViewCell";

NSInteger const paginationSection = 1;

@interface PKTListPokemonViewController ()

@property (nonatomic, strong) PKTListPokemonViewModel *viewModel;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation PKTListPokemonViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addObservers];
    [self configureViewModel];
    [self configureCollectionView];
}

- (void)dealloc {

    [self removeObservers];
}

- (void)configureViewModel {
   
    self.viewModel = [PKTListPokemonViewModel new];
}

- (void)configureCollectionView {
    
    self.refreshControl = [UIRefreshControl new];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(fetchPokemons) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    
    [self.collectionView registerNib:[UINib nibWithNibName:commonCellIdentifier bundle:nil] forCellWithReuseIdentifier:commonCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:loadingCellIdentifier bundle:nil] forCellWithReuseIdentifier:loadingCellIdentifier];
}

- (void)fetchPokemons {
    
    [self.viewModel fetchPokemons];
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return [self.viewModel shouldGetNext] ? 2 : 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return section == paginationSection ? 1 : [self.viewModel countPokemons];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == paginationSection) {
        
        PKTLoadingCollectionViewCell *loadingCell = [collectionView dequeueReusableCellWithReuseIdentifier:loadingCellIdentifier forIndexPath:indexPath];
        return loadingCell;
        
    } else {
        
        PKTCommonCollectionViewCell *collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:commonCellIdentifier forIndexPath:indexPath];
        [collectionCell clean];
        collectionCell.nameLabel.text = [self.viewModel pokemonNameForIndexPath:indexPath];
        
        //Async image downloader with image cache category from AFNetworking - pretty cool
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[self.viewModel pokemonImageForIndexPath:indexPath]
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [collectionCell.imageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"placeholder"] success:nil failure:nil];
        
        return collectionCell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == paginationSection) {
        PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)cell;
        [loadingCell.activityIndicator startAnimating];
        [self.viewModel getMorePokemons];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == paginationSection) {
        CGFloat cellWidth = CGRectGetWidth(self.collectionView.frame);
        CGFloat cellHeight = 50;
        return CGSizeMake(cellWidth, cellHeight);
    } else {
        NSInteger numberOfCellsPerRow = 3;
        CGFloat cellWidth = CGRectGetWidth(self.collectionView.frame) / numberOfCellsPerRow;
        CGFloat cellHeight = cellWidth + 15;
        
        return CGSizeMake(cellWidth, cellHeight);
    }
}

#pragma mark - refresh

- (void)refresh {
    
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
    
    PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [loadingCell.activityIndicator stopAnimating];
}

- (void)refreshFailed:(NSNotification*)notification {
    
    NSString *errorMessage = notification.object;
    [self.refreshControl endRefreshing];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [loadingCell.activityIndicator stopAnimating];
}

- (void)refreshEnd {
    
    [self.refreshControl endRefreshing];
    PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [loadingCell.activityIndicator stopAnimating];
    
    [self.collectionView reloadData];
}

#pragma mark - Observers

- (void)addObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:pokemonListDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:pokemonListDidFailed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshEnd) name:pokemonListDidEnd object:nil];
}

- (void)removeObservers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:pokemonListDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:pokemonListDidFailed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:pokemonListDidEnd object:nil];
}

@end
