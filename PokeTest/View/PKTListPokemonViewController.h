//
//  PKTListPokemonViewController.h
//  PokeTest
//
//  Created by Tiago Bencardino on 18/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKTListPokemonViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
