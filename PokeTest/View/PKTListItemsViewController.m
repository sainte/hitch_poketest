//
//  PKTListItemsViewController.m
//  PokeTest
//
//  Created by Tiago Bencardino on 18/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTListItemsViewController.h"
#import "PKTListItemsViewModel.h"
#import "PKTCommonCollectionViewCell.h"
#import "PKTLoadingCollectionViewCell.h"

#import "UIImageView+AFNetworking.h"
#import "UIImageView+PKTClearCache.h"

NSString * const commonItemCellIdentifier = @"PKTCommonCollectionViewCell";
NSString * const loadingItemCellIdentifier = @"PKTLoadingCollectionViewCell";

NSInteger const paginationItemSection = 1;

@interface PKTListItemsViewController ()

@property (nonatomic, strong) PKTListItemsViewModel *viewModel;
@property (nonatomic, strong) UIRefreshControl *refreshControl;


@end

@implementation PKTListItemsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addObservers];
    [self configureViewModel];
    [self configureCollectionView];
}

- (void)dealloc {
    
    [self removeObservers];
}

- (void)configureViewModel {
    
    self.viewModel = [PKTListItemsViewModel new];
}

- (void)configureCollectionView {
    
    self.refreshControl = [UIRefreshControl new];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(fetchPokemons) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    
    [self.collectionView registerNib:[UINib nibWithNibName:commonItemCellIdentifier bundle:nil] forCellWithReuseIdentifier:commonItemCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:loadingItemCellIdentifier bundle:nil] forCellWithReuseIdentifier:loadingItemCellIdentifier];
}

- (void)fetchPokemons {
    
    [self.viewModel fetchItems];
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return [self.viewModel shouldGetNext] ? 2 : 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return section == paginationItemSection ? 1 : [self.viewModel countItems];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == paginationItemSection) {
        
        PKTLoadingCollectionViewCell *loadingCell = [collectionView dequeueReusableCellWithReuseIdentifier:loadingItemCellIdentifier forIndexPath:indexPath];
        return loadingCell;
        
    } else {
        
        PKTCommonCollectionViewCell *collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:commonItemCellIdentifier forIndexPath:indexPath];
        [collectionCell clean];
        collectionCell.nameLabel.text = [self.viewModel itemNameForIndexPath:indexPath];
        
        //Async image downloader with image cache category from AFNetworking - pretty cool
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[self.viewModel itemImageForIndexPath:indexPath]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [collectionCell.imageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"placeholder"] success:nil failure:nil];
        
        return collectionCell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == paginationItemSection) {
        PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)cell;
        [loadingCell.activityIndicator startAnimating];
        [self.viewModel getMoreItems];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == paginationItemSection) {
        CGFloat cellWidth = CGRectGetWidth(self.collectionView.frame);
        CGFloat cellHeight = 50;
        return CGSizeMake(cellWidth, cellHeight);
    } else {
        NSInteger numberOfCellsPerRow = 4;
        CGFloat cellWidth = CGRectGetWidth(self.collectionView.frame) / numberOfCellsPerRow;
        CGFloat cellHeight = cellWidth + 15;
        
        return CGSizeMake(cellWidth, cellHeight);
    }
}

#pragma mark - refresh

- (void)refresh {
    
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
    
    PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [loadingCell.activityIndicator stopAnimating];
}

- (void)refreshFailed:(NSNotification*)notification {
    
    NSString *errorMessage = notification.object;
    [self.refreshControl endRefreshing];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [loadingCell.activityIndicator stopAnimating];
}

- (void)refreshEnd {
    
    [self.refreshControl endRefreshing];
    PKTLoadingCollectionViewCell *loadingCell = (PKTLoadingCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [loadingCell.activityIndicator stopAnimating];
    
    [self.collectionView reloadData];
}

#pragma mark - Observers

- (void)addObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:itemListDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFailed:) name:itemListDidFailed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshEnd) name:itemListDidEnd object:nil];
}

- (void)removeObservers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:itemListDidChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:itemListDidFailed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:itemListDidEnd object:nil];
}

@end
