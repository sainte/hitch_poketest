//
//  PKTLoadingCollectionViewCell.h
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKTLoadingCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
