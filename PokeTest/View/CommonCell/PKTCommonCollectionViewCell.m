//
//  PKTCommonCollectionViewCell.m
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTCommonCollectionViewCell.h"

@implementation PKTCommonCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)clean {
    
    self.imageView.image = [UIImage imageNamed:@"placeholder"];
    self.nameLabel.text = @"";
}
@end
