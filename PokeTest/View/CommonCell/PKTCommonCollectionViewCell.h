//
//  PKTCommonCollectionViewCell.h
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKTCommonCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

- (void)clean;

@end
