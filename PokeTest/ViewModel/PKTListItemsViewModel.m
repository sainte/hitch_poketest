//
//  PKTListItemsViewModel.m
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTListItemsViewModel.h"
#import "PKTPokeAPIService.h"
#import "NSError+PKTErrorMessage.h"
#import "PKTItem.h"

@interface PKTListItemsViewModel()

@property (nonatomic, strong) NSString *next;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) PKTPokeAPIService *api;

@end

NSString * const itemListDidChanged = @"itemListDidChanged";
NSString * const itemListDidFailed = @"itemListDidFailed";
NSString * const itemListDidEnd = @"itemListDidEnd";

@implementation PKTListItemsViewModel

- (id)init {
    
    if (self = [super init]) {
        self.api = [PKTPokeAPIService new];
    }
    return self;
}

- (id)initWithItems:(NSArray *)items {
    
    if (self = [self init]) {
        self.items = [[NSMutableArray alloc] initWithArray:items];
    }
    return self;
}

- (void)fetchItems {
    
    self.next = nil;
    [self getMoreItems];
}

- (void)getMoreItems {
    
    if ([self shouldGetNext]) {
        [self.api getItemsWithNext:self.next success:^(id items, NSString *next) {
            
            if (!self.next) {
                self.items = [NSMutableArray new];
            }
            [self.items addObjectsFromArray:items];
            
            self.next = next;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:itemListDidChanged object:self.items];
        } failure:^(NSError *error) {
            
            NSString *errorMessage = [error pkt_errorMessage];
            [[NSNotificationCenter defaultCenter] postNotificationName:itemListDidFailed object:errorMessage];
        }];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:itemListDidEnd object:nil];
    }
}

- (NSInteger)countItems {
    
    return self.items.count;
}

- (BOOL)shouldGetNext {
    
    return ![self.next isEqualToString:paginationEnd];
}

#pragma mark - data binding

- (NSString*)itemNameForIndexPath:(NSIndexPath *)indexPath {
    
    PKTItem *item = self.items[indexPath.row];
    NSString *name = item.name;
    
    name = [[name stringByReplacingOccurrencesOfString:@"-" withString:@" "] capitalizedString];
    
    return name;
}

- (NSURL*)itemImageForIndexPath:(NSIndexPath *)indexPath {
    
    PKTItem *item = self.items[indexPath.row];
    return item.imageURL;
}

@end
