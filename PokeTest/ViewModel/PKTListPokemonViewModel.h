//
//  PKTListPokemonViewModel.h
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const pokemonListDidChanged;
extern NSString * const pokemonListDidFailed;
extern NSString * const pokemonListDidEnd;

@interface PKTListPokemonViewModel : NSObject

- (id)initWithPokemons:(NSArray*)pokemons;

- (void)fetchPokemons;
- (void)getMorePokemons;
- (NSInteger)countPokemons;

- (BOOL)shouldGetNext;

#pragma mark - data binding

- (NSString*)pokemonNameForIndexPath:(NSIndexPath*)indexPath;
- (NSURL*)pokemonImageForIndexPath:(NSIndexPath*)indexPath;

@end
