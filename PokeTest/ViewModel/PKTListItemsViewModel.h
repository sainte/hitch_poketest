//
//  PKTListItemsViewModel.h
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const itemListDidChanged;
extern NSString * const itemListDidFailed;
extern NSString * const itemListDidEnd;

@interface PKTListItemsViewModel : NSObject

- (id)initWithItems:(NSArray*)items;

- (void)fetchItems;
- (void)getMoreItems;

- (BOOL)shouldGetNext;
- (NSInteger)countItems;

#pragma mark - data binding

- (NSString*)itemNameForIndexPath:(NSIndexPath *)indexPath;
- (NSURL*)itemImageForIndexPath:(NSIndexPath *)indexPath;

@end
