//
//  PKTListPokemonViewModel.m
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTListPokemonViewModel.h"
#import "PKTPokeAPIService.h"
#import "NSError+PKTErrorMessage.h"
#import "PKTPokemon.h"

@interface PKTListPokemonViewModel ()

@property (nonatomic, strong) NSString *next;
@property (nonatomic, strong) NSMutableArray *pokemons;
@property (nonatomic, strong) PKTPokeAPIService *api;

@end

NSString * const pokemonListDidChanged = @"pokemonListDidChanged";
NSString * const pokemonListDidFailed = @"pokemonListDidFailed";
NSString * const pokemonListDidEnd = @"pokemonListDidEnd";

@implementation PKTListPokemonViewModel

- (id)init {
    
    if (self = [super init]) {
        self.api = [PKTPokeAPIService new];
    }
    return self;
}

- (id)initWithPokemons:(NSArray*)pokemons {
 
    if (self = [self init]) {
        self.pokemons = [[NSMutableArray alloc] initWithArray:pokemons];
    }
    return self;
}

- (void)fetchPokemons {
    
    self.next = nil;
    [self getMorePokemons];
}

- (void)getMorePokemons {
    
    if ([self shouldGetNext]) {
        [self.api getPokemonsWithNext:self.next success:^(id pokemons, NSString *next) {
            
            if (!self.next) {
                self.pokemons = [NSMutableArray new];
            }
            [self.pokemons addObjectsFromArray:pokemons];
            
            self.next = next;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:pokemonListDidChanged object:self.pokemons];
        } failure:^(NSError *error) {
            
            NSString *errorMessage = [error pkt_errorMessage];
            [[NSNotificationCenter defaultCenter] postNotificationName:pokemonListDidFailed object:errorMessage];
        }];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:pokemonListDidEnd object:nil];
    }
}

- (NSInteger)countPokemons {
    
    return self.pokemons.count;
}

- (BOOL)shouldGetNext {
 
    return ![self.next isEqualToString:paginationEnd];
}

- (NSString*)pokemonNameForIndexPath:(NSIndexPath *)indexPath {
    
    PKTPokemon *pokemon = self.pokemons[indexPath.row];
    return [pokemon.name capitalizedString];
}

- (NSURL*)pokemonImageForIndexPath:(NSIndexPath *)indexPath {
    
    PKTPokemon *pokemon = self.pokemons[indexPath.row];
    return pokemon.imageURL;
}

@end
