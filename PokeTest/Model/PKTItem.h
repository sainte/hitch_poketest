//
//  PKTItem.h
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PKTItem : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSURL *imageURL;

@end
