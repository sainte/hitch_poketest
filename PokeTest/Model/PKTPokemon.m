//
//  PKTPokemon.m
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTPokemon.h"

@interface PKTPokemon()

@property (nonatomic, strong) NSURL *url;

@end

@implementation PKTPokemon

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    
    return @{
             @"name" : @"name",
             @"url" : @"url"
             };
}

- (NSNumber*)ID {
    
    if (!_ID) {
        NSString *idString = [[self.url pathComponents] lastObject];
       _ID = @([idString integerValue]);
    }
    
    return _ID;
}

- (NSURL*)imageURL {
    
    NSString *image = [NSString stringWithFormat:@"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/%@.png", self.ID];
    return [NSURL URLWithString:image];
}

- (NSURL*)shinyImageURL {
    
    NSString *image =  [NSString stringWithFormat:@"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/%@.png", self.ID];
    return [NSURL URLWithString:image];
}

@end
