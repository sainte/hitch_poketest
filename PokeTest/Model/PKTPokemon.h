//
//  PKTPokemon.h
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mantle/Mantle.h>

@interface PKTPokemon : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSURL *shinyImageURL;

@end
