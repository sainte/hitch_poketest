//
//  PKTItem.m
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTItem.h"

@interface PKTItem()

@property (nonatomic, strong) NSURL *url;

@end

@implementation PKTItem

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    
    return @{
             @"name" : @"name",
             @"url" : @"url"
             };
}

- (NSNumber*)ID {
    
    if (!_ID) {
        NSString *idString = [[self.url pathComponents] lastObject];
        _ID = @([idString integerValue]);
    }
    
    return _ID;
}

- (NSURL*)imageURL {
    
    NSString *image = [NSString stringWithFormat:@"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/%@.png", self.name];
    return [NSURL URLWithString:image];
}

@end
