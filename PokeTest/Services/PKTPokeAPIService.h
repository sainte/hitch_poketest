//
//  PKTPokeAPIService.h
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^successResponse)(id object, NSString *next);
typedef void(^failureResponse)(NSError *error);

extern NSString * const paginationEnd;

@interface PKTPokeAPIService : NSObject

- (void)getPokemonsWithNext:(NSString*)next success:(successResponse)success failure:(failureResponse)failure;

- (void)getItemsWithNext:(NSString*)next success:(successResponse)success failure:(failureResponse)failure;

@end
