//
//  PKTPokeAPIService.m
//  PokeTest
//
//  Created by Tiago Bencardino on 19/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "PKTPokeAPIService.h"
#import <AFNetworking/AFNetworking.h>
#import "PKTPokemon.h"
#import "PKTItem.h"

@interface PKTPokeAPIService()

@end

NSString * const pokemonListLink = @"http://pokeapi.co/api/v2/pokemon?limit=20";
NSString * const itemListLink = @"http://pokeapi.co/api/v2/item?limit=40";

NSString * const paginationEnd = @"pagination_end";

@implementation PKTPokeAPIService

- (void)getPokemonsWithNext:(NSString*)next success:(successResponse)success failure:(failureResponse)failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *path = next ? next : pokemonListLink;
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error;
        NSArray *pokemons = [MTLJSONAdapter modelsOfClass:[PKTPokemon class] fromJSONArray:responseObject[@"results"] error:&error];
        if (error) {
            failure(error);
        } else {
            NSString *next = responseObject[@"next"];
            if ([next isKindOfClass:[NSNull class]]) {
                next = paginationEnd;
            }
            success(pokemons, next);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

- (void)getItemsWithNext:(NSString*)next success:(successResponse)success failure:(failureResponse)failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *path = next ? next : itemListLink;
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error;
        NSArray *items = [MTLJSONAdapter modelsOfClass:[PKTItem class] fromJSONArray:responseObject[@"results"] error:&error];
        if (error) {
            failure(error);
        } else {
            NSString *next = responseObject[@"next"];
            if ([next isKindOfClass:[NSNull class]]) {
                next = paginationEnd;
            }
            success(items, next);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}



@end
