//
//  UIImageView+PKTClearCache.m
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import "UIImageView+PKTClearCache.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFImageDownloader.h>
#import <AFNetworking/AFAutoPurgingImageCache.h>

@implementation UIImageView (PKTClearCache)

+ (void)pkt_clearImageCacheForURL:(NSURL *)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    id <AFImageRequestCache> imageCache = [[self class] sharedImageDownloader].imageCache;
    UIImage *cachedImage = [imageCache imageforRequest:request withAdditionalIdentifier:nil];
    if (cachedImage) {
        [self pkt_clearCached:imageCache Request:request];
    }
}

+ (void)pkt_clearCached:(AFAutoPurgingImageCache *)imageCache Request:(NSURLRequest *)request {
    if (request) {
        [imageCache removeImageforRequest:request withAdditionalIdentifier:nil];
    }
}
@end
