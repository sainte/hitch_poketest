//
//  NSError+PKTErrorMessage.h
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (PKTErrorMessage)

- (NSString*)pkt_errorMessage;

@end
