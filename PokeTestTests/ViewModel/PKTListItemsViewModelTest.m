//
//  PKTListItemsViewModelTest.m
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PKTListItemsViewModel.h"
#import "PKTItem.h"
#import <Mantle/Mantle.h> //Expecta

@interface PKTListItemsViewModelTest : XCTestCase

@property (nonatomic, strong) PKTListItemsViewModel *viewModel;

@end

@implementation PKTListItemsViewModelTest

#pragma mark - static load

static NSDictionary *itemList;

+ (void)setUp {
    [super setUp];
    [self loadJSONResponse];
    
}
/** This static method loads our json data once. No need to repeat this step for each test. */
+ (void)loadJSONResponse {
    
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSURL *pokemonListJSONURL = [testBundle URLForResource:@"itemListResponse" withExtension:@"json"];
    NSData *data = [NSData dataWithContentsOfURL:pokemonListJSONURL options:kNilOptions error:nil];
    itemList = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}


- (void)setUp {
    [super setUp];
    
    NSArray *items = [MTLJSONAdapter modelsOfClass:[PKTItem class] fromJSONArray:itemList[@"results"] error:nil];
    self.viewModel = [[PKTListItemsViewModel alloc] initWithItems:items];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - test binding

- (void)testCount {
    
    NSInteger expectedCount = 40;
    NSInteger value = [self.viewModel countItems];
    
    XCTAssertEqual(expectedCount, value);
}
- (void)testItemNameForIndexPathIsCorrectAndCapitalizedWithoutDash {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    NSString *expectedValue = @"Poke Ball";
    NSString *value = [self.viewModel itemNameForIndexPath:indexPath];
    
    XCTAssertTrue([value isEqualToString:expectedValue], @"Item name is not the expected.");
}

- (void)testItemImageForIndexPathMountedTheRightLink {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    NSString *expectedValue = [@"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png" description];
    NSString *value = [[[self.viewModel itemImageForIndexPath:indexPath] absoluteString] description];
    
    XCTAssertTrue([value isEqualToString:expectedValue], @"Item image url is not the expected.");
}
@end
