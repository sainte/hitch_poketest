//
//  PKTListPokemonViewModelTest.m
//  PokeTest
//
//  Created by Tiago Bencardino on 20/10/16.
//  Copyright © 2016 Hitch. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PKTListPokemonViewModel.h"
#import "PKTPokemon.h"
#import <Mantle/Mantle.h> //Expecta

@interface PKTListPokemonViewModelTest : XCTestCase

@property (nonatomic, strong) PKTListPokemonViewModel *viewModel;

@end

@implementation PKTListPokemonViewModelTest

#pragma mark - static load

static NSDictionary *pokemonList;

+ (void)setUp {
    [super setUp];
    [self loadJSONResponse];
    
}
/** This static method loads our json data once. No need to repeat this step for each test. */
+ (void)loadJSONResponse {
    
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSURL *pokemonListJSONURL = [testBundle URLForResource:@"pokemonListResponse" withExtension:@"json"];
    NSData *data = [NSData dataWithContentsOfURL:pokemonListJSONURL options:kNilOptions error:nil];
    pokemonList = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

#pragma mark - dynamic setup

- (void)setUp {
    
    [super setUp];
    
    NSArray *pokemons = [MTLJSONAdapter modelsOfClass:[PKTPokemon class] fromJSONArray:pokemonList[@"results"] error:nil];
    self.viewModel = [[PKTListPokemonViewModel alloc] initWithPokemons:pokemons];
}

- (void)tearDown {
    
    [super tearDown];
}

#pragma mark - test binding

- (void)testCount {
    
    NSInteger expectedCount = 20;
    NSInteger value = [self.viewModel countPokemons];
    
    XCTAssertEqual(expectedCount, value);
}
- (void)testPokemonNameForIndexPathIsCorrectAndCapitalized {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
    NSString *expectedValue = @"Squirtle";
    NSString *value = [self.viewModel pokemonNameForIndexPath:indexPath];
    
    XCTAssertTrue([value isEqualToString:expectedValue], @"Pokémon name is not the expected.");
}

- (void)testPokemonImageForIndexPathMountedTheRightLink {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
    NSString *expectedValue = [@"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png" description];
    NSString *value = [[[self.viewModel pokemonImageForIndexPath:indexPath] absoluteString] description];
    
    XCTAssertTrue([value isEqualToString:expectedValue], @"Pokémon image url is not the expected.");
}

@end
